from selenium.webdriver.common.action_chains import ActionChains

class ToDoMain(object):
    def __init__(self, driver, main_url):
        self.base_url = main_url
        self.driver = driver
        self.timeout = 15

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def find_elements(self, *locator):
        return self.driver.find_elements(*locator)

    def open(self, url):
        url = self.base_url + url
        self.driver.get(url)

    def gettitle(self):
        return self.driver.title

    def geturl(self):
        return self.driver.current_url

    def hover(self, *locator):
        element = self.find_element(*locator)
        hover = ActionChains(self.driver).move_to_element(element)
        hover.perform()

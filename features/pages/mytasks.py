from features.pages.todomain import ToDoMain
from features.pages.navigationbar import navigationbarpage
from features.pages.page_locators import *

class mytasks_page(ToDoMain, navigationbarpage):
    def __init__(self, driver, main_url):
        ToDoMain.__init__(self, driver, main_url)

    def check_todolistmsg(self, actual):
        return True if (self.find_element(*mytasks_locators.TODOMSG).text == actual) else False

    def setnewtaskdescription(self, taskdesc):
        self.find_element(*mytasks_locators.NEW_TASK_DESC).send_keys(taskdesc)

    def click_addtask(self):
        self.find_element(*mytasks_locators.ADD_TASK).click()

    def getMyTasksElements(self):
        tb = self.find_element(*mytasks_locators.TABLE)
        checkboxes = self.driver.find_elements(*mytasks_locators.CHKBOX)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(0, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                print(columns[j].text)
                if ("Manage Subtasks" in (columns[j].text)):
                    columns[j].click()

    def select_checkbox_done(self, taskdesc):
        tb = self.find_element(*mytasks_locators.TABLE)
        checkboxes = self.driver.find_elements(*mytasks_locators.CHKBOX)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(0, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                if (columns[j].text == taskdesc):
                    if (i % 2 != 0):
                        checkboxes[i-1].click()
                    else:
                        checkboxes[i].click()
                    break

    def select_checkbox_public(self, taskdesc):
        tb = self.find_element(*mytasks_locators.TABLE)
        checkboxes = self.driver.find_elements(*mytasks_locators.CHKBOX)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(1, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                if (columns[j].text == taskdesc):
                    if (i % 2 != 0):
                        checkboxes[i].click()
                    else:
                        checkboxes[i+1].click()
                    break

    def click_todolink(self, taskdesc):
        tb = self.find_element(*mytasks_locators.TABLE)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(1, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                if (columns[j].text == taskdesc):
                    self.find_element(*(
                    By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(i) + ']/td[2]/a')).click()
                    return(i)
        return(0)

    def click_managesubtasks(self, taskdesc):
        tb = self.find_element(*mytasks_locators.TABLE)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(1, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                if (columns[j].text == taskdesc):
                    self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(i) + ']/td[4]/button')).click()
                    break

    def verify_taskfromtodolist(self, taskdesc):
        tb = self.find_element(*mytasks_locators.TABLE)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(1, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                if (columns[j].text == taskdesc):
                    return (True)
        return False

    def getsubtaskcount(self, taskdesc):
        tb = self.find_element(*mytasks_locators.TABLE)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(1, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                if (columns[j].text == taskdesc):
                    count = self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(i) + ']/td[4]/button')).text
                    return (count.split(')')[0].split('(')[1])
        return None

    def click_remove(self, taskdesc):
        tb = self.find_element(*mytasks_locators.TABLE)
        rows = tb.find_elements(*mytasks_locators.ROW)
        for i in range(1, len(rows)):
            columns = rows[i].find_elements(*mytasks_locators.COLUMN)
            for j in range(0, len(columns)):
                if (columns[j].text == taskdesc):
                    self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(i) + ']/td[5]/button')).click()
                    break

    def update_taskdesc_accept(self, r, newtaskdesc):
        self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(r) + ']/td[2]/form/div/input')).clear()
        self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(r) + ']/td[2]/form/div/input')).send_keys(newtaskdesc)
        self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(r) + ']/td[2]/form/div/span/button[1]')).click()

    def update_taskdesc_cancel(self, r, newtaskdesc):
        self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(r) + ']/td[2]/form/div/input')).clear()
        self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(r) + ']/td[2]/form/div/input')).send_keys(newtaskdesc)
        self.find_element(*(By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[' + str(r) + ']/td[2]/form/div/span/button[2]')).click()

from features.pages.page_locators import *
from selenium.common.exceptions import *

class navigationbarpage(object):
    def __init__(self, driver, main_url):
        self.base_url = main_url
        self.driver = driver

    def click_signin(self):
        self.driver.find_element(*navbar_locators.NAVBAR_SIGNIN).click()

    def click_signout(self):
        try:
            self.driver.find_element(*homepage_locators.HEADER_SIGNOUT).click()
        except NoSuchElementException:
            print("Signout link doesn't exist")

    def click_register(self):
        self.driver.find_element(*navbar_locators.NAVBAR_REGISTER).click()

    def click_todoapp(self):
        self.driver.find_element(*navbar_locators.NAVBAR_TODOAPP).click()

    def click_home(self):
        self.driver.find_element(*navbar_locators.NAVBAR_HOME).click()

    def click_mytasks(self):
        self.driver.find_element(*navbar_locators.NAVBAR_MYTASKS).click()

    def check_developedbytext(self, actual):
        return True if (self.driver.find_element(*navbar_locators.FOOTER_DEVBY).text == actual) else False

    def click_developedby(self):
        self.driver.find_element(*navbar_locators.FOOTER_AUTHOR).click()

    def click_testdesc(self):
        self.driver.find_element(*navbar_locators.FOOTER_TESTDESC).click()

    def click_trackbugs(self):
        self.driver.find_element(*navbar_locators.FOOTER_BUGS).click()

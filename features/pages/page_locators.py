from selenium.webdriver.common.by import By

# some of the below xpath values might become dynamic based on the <div> tags. If the changes are minimal,
# manually update else write a way to loop through and find the class etc.
# another way is to request developers to add ID or other identifiable tags in code

class navbar_locators(object):
    NAVBAR_TODOAPP = (By.XPATH, '/html/body/div[1]/div[1]/div/div[1]/a')
    NAVBAR_HOME = (By.XPATH, '/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[1]/a')
    NAVBAR_MYTASKS = (By.XPATH, '/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a')
    NAVBAR_REGISTER = (By.XPATH, '/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[2]/a')
    FOOTER_DEVBY = (By.XPATH,'/html/body/div[2]/div/p/text()[1]')
    NAVBAR_SIGNIN = (By.LINK_TEXT, 'Sign In')
    FOOTER_AUTHOR = (By.LINK_TEXT,'Avenue Code')
    FOOTER_TESTDESC = (By.LINK_TEXT, 'Test Description - User Stories')
    FOOTER_BUGS = (By.LINK_TEXT,'Track a Bug')

class firsthomepage_locators(object):
    APP_TITLE = (By.XPATH, '/html/body/div[1]/div[2]/center/h1')
    APP_DESC = (By.XPATH,'/html/body/div[1]/div[2]/center/p')
    APP_SIGNUP = (By.LINK_TEXT, 'Sign Up')
    APP_TESTDESC = (By.LINK_TEXT,'Test Description')
    TITLE = (By.XPATH, '/html/head/title')

class signinpage_locators(object):
    USERID = (By.ID, 'user_email')
    PWD = (By.ID, 'user_password')
    SUBMIT = (By.NAME, 'commit')
    REMEMBERME = (By.ID, 'user_remember_me')
    INVALIDMSG = (By.CSS_SELECTOR, 'body > div.container > div.alert.alert-warning')

class homepage_locators(object):
    HEADER_WELCOME = (By.XPATH,'/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a')
    HEADER_SIGNOUT = (By.LINK_TEXT,'Sign out')
    MSG_SUCCESS = (By.XPATH,'/html/body/div[1]/div[2]')
    MYTASKS = (By.XPATH,'/html/body/div[1]/div[3]/center/a')

class mytasks_locators(object):
    TODOMSG = (By.XPATH,'/html/body/div[1]/h1')
    NEW_TASK_DESC = (By.ID,'new_task')
    ADD_TASK = (By.XPATH,'/html/body/div[1]/div[2]/div[1]/form/div[2]/span')
    TOBEDONE = (By.XPATH, '/html/body/div[1]/div[2]/div[2]/div/div')
    TABLE = (By.CLASS_NAME, 'table')
    ROW = (By.TAG_NAME, 'tr')
    COLUMN = (By.TAG_NAME, 'td')
    CHKBOX = (By.XPATH, '//table[@class="table"]//input[@type="checkbox"]')

class mysubtasks_locators(object):
    EDIT_TASK = (By.XPATH,'/html/body/div[4]/div/div/div[1]/h3')
    EDITTASKTEXT = (By.ID,'edit_task')
    SUBTASKTEXT = (By.ID, 'new_sub_task')
    DUEDATE = (By.ID, 'dueDate')
    ADDSUBTASK = (By.ID, 'add-subtask')
    CLOSE = (By.XPATH,'/html/body/div[4]/div/div/div[3]/button')
    TABLE = (By.CLASS_NAME, 'table')

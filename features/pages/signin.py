from features.pages.todomain import ToDoMain
from features.pages.navigationbar import navigationbarpage
from features.pages.page_locators import *

class signinpage(ToDoMain, navigationbarpage):
    def __init__(self, driver, main_url):
        ToDoMain.__init__(self, driver, main_url)

    def setuserid(self, userid):
        self.find_element(*signinpage_locators.USERID).send_keys(userid)

    def setpassword(self, password):
        self.find_element(*signinpage_locators.PWD).send_keys(password)

    def setrememberme(self):
        self.find_element(*signinpage_locators.REMEMBERME).click()

    def click_btnsignin(self):
        self.find_element(*signinpage_locators.SUBMIT).click()

    def check_invalidcredentials(self, actual):
        return True if (self.find_element(*signinpage_locators.INVALIDMSG).text == actual) else False

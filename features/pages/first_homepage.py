from features.pages.todomain import ToDoMain
from features.pages.navigationbar import navigationbarpage
from features.pages.page_locators import *

class first_homepage(ToDoMain, navigationbarpage):
    def __init__(self, driver, main_url):
        ToDoMain.__init__(self, driver, main_url)

    def check_title(self, actual):
        return True if (self.find_element(*firsthomepage_locators.TITLE).text == actual) else False

    def click_signup(self):
        self.find_element(*firsthomepage_locators.APP_SIGNUP).click()

    def click_testdescription(self):
        self.find_element(*firsthomepage_locators.APP_TESTDESC).click()

    def check_apptitle(self, actual):
        return True if (self.find_element(*firsthomepage_locators.APP_TITLE).text == actual) else False

    def check_appdesc(self, actual):
        return True if (self.find_element(*firsthomepage_locators.APP_DESC).text == actual) else False

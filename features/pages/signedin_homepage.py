from features.pages.todomain import ToDoMain
from features.pages.navigationbar import navigationbarpage
from features.pages.page_locators import *

class signedinhomepage(ToDoMain, navigationbarpage):
    def __init__(self, driver, main_url):
        ToDoMain.__init__(self, driver, main_url)

    def click_mytasks(self):
        self.find_element(*homepage_locators.MYTASKS).click()

    def check_welcomemsg(self, actual):
        return True if (self.find_element(*homepage_locators.HEADER_WELCOME).text == actual) else False

    def check_signinsuccess(self, actual):
        return True if (self.find_element(*homepage_locators.MSG_SUCCESS).text == actual) else False

from features.browser import Browser
from features.pages.navigationbar import *
import pyautogui
import datetime
import os

def before_all(context):
    context.browser = Browser()

def after_step(context, step):
    if step.status == 'failed':
        errormsg_jpg = pyautogui.screenshot(os.path.join(os.path.dirname(__file__),"..\\screenshots\\" + step.name + datetime.datetime.now().strftime("_%Y%m%d_%H%M%S") + ".jpg"))
        n = navigationbarpage(context.browser.driver,'')
        n.click_signout()

def after_scenario(context, scenario):
    print(scenario.status)

def after_all(context):
    context.browser.close()

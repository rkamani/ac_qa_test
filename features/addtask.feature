Feature: AddTask

	Scenario: Add a new task
		Given I navigate to the ToDoApp First Home page
		And I set login credentials with index 0 and sign in
		And I click on My Tasks button
		When I add a new task
		Then A new task is added in to do list page
		And I am signed out

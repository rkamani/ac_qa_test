Feature: MyTasks

	Scenario: Open and Verify MyTasks
		Given I navigate to the ToDoApp First Home page
		And I set login credentials with index 0 and sign in
		When I click on My Tasks button
		Then I am taken to user to do list page
		And I am signed out

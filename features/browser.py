from selenium import webdriver
import os


class Browser(object):
    _TIMEOUT = 15
    driver = webdriver.Chrome(os.path.join(os.path.dirname(__file__),"..\\tools\\chromedriver.exe"))
    driver.implicitly_wait(_TIMEOUT)
    driver.set_page_load_timeout(_TIMEOUT)
    driver.maximize_window()

    def close(context):
        context.driver.close()

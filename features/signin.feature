Feature: SignIn

	Scenario: Login to ToDoApp
		Given I navigate to the ToDoApp First Home page
		When I set login credentials with index 0 and sign in
		Then I am taken to the ToDoApp Home page
		And I am signed out

	Scenario: Login to ToDoApp
		Given I navigate to the ToDoApp First Home page
		When I set login credentials with index 1 and sign in
		Then I verify error message for invalid credentials

from behave import *
from features.pages.first_homepage import *
from features.pages.signin import *
from features.pages.signedin_homepage import *
import base64
import json
import os

# get application URL and title from data file
f=open(os.path.join(os.path.dirname(__file__), '..\\testdata\\todomaindata.json'))
todomaindata = json.load(f)

# step to navigate to application first home page and then to Sign In page
@step('I navigate to the ToDoApp First Home page')
def step_impl(context):
    fhg = first_homepage(context.browser.driver, todomaindata[0]['url'])
    fhg.open('')
    assert (fhg.gettitle() == todomaindata[0]['title'])
    fhg.click_signin()

# step to enter valid user credentials and sign in successfully, assert info message after login
@step('I set login credentials with index {dataindex} and sign in')
def step_impl(context, dataindex):
    with open(os.path.join(os.path.dirname(__file__),'..\\testdata\\signindata.json')) as fl:
        signindata = json.load(fl)
        m = signinpage(context.browser.driver, todomaindata[0]['url'])
        m.setuserid(signindata[int(dataindex)]['userid'])
        m.setpassword(base64.b64decode(signindata[int(dataindex)]['password']).decode("utf-8"))
        m.click_btnsignin()

# step to verify invalid login message
@step('I verify error message for invalid credentials')
def step_impl(context):
    h = signinpage(context.browser.driver, todomaindata[0]['url'])
    assert h.check_invalidcredentials("Invalid email or password.")

# step to sign out of the application
@step('I am taken to the ToDoApp Home page')
def step_impl(context):
    h = signedinhomepage(context.browser.driver, todomaindata[0]['url'])
    assert h.check_welcomemsg("Welcome, Rames Kamani!")
    assert h.check_signinsuccess("Signed in successfully.")

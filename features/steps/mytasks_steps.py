from behave import *
from features.pages.signedin_homepage import *
from features.pages.mytasks import *
import json
import os

# get application URL and title from data file
f=open(os.path.join(os.path.dirname(__file__), '..\\testdata\\todomaindata.json'))
todomaindata = json.load(f)

# step to click on my tasks button
@step('I click on My Tasks button')
def step_impl(context):
    mt = signedinhomepage(context.browser.driver, todomaindata[0]['url'])
    mt.click_mytasks()

# step to add a new task
@step('I add a new task')
def step_impl(context):
    mt = mytasks_page(context.browser.driver, todomaindata[0]['url'])
    mt.setnewtaskdescription("my first new task")
    mt.click_addtask()

# step to verify new task added
@step('A new task is added in to do list page')
def step_impl(context):
    tdl = mytasks_page(context.browser.driver, todomaindata[0]['url'])
    assert tdl.check_todolistmsg("Rames Kamani's ToDo List")
    assert tdl.verify_taskfromtodolist("my first new task")

# step to verify message
@step('I am taken to user to do list page')
def step_impl(context):
    tdl = mytasks_page(context.browser.driver, todomaindata[0]['url'])
    assert tdl.check_todolistmsg("Hey  Rames Kamani, this is your todo list for today:")

# step to sign out of the application
@step('I am signed out')
def step_impl(context):
    nv = navigationbarpage(context.browser.driver, todomaindata[0]['url'])
    nv.click_signout()
